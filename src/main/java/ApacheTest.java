import org.ahocorasick.trie.PayloadEmit;
import org.ahocorasick.trie.PayloadTrie;
import org.ahocorasick.trie.Token;
import org.ahocorasick.trie.Trie;

import java.util.Collection;

public class ApacheTest {

    public static void main(String[] args) {
        class Word {
            private final String gender;
            public Word(String gender) {
                this.gender = gender;
            }
        }
        PayloadTrie<Word> trie = PayloadTrie.<Word>builder()
                .addKeyword("hers", new Word("f"))
                .addKeyword("his", new Word("m"))
                .addKeyword("she", new Word("f"))
                .addKeyword("he", new Word("m"))
                .addKeyword("nonbinary", new Word("nb"))
                .addKeyword("transgender", new Word("tg"))
                .build();
        Collection<PayloadEmit<Word>> emits = trie.parseText("ushers");
        System.out.println(emits.toString());
    }
}
